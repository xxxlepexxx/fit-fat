package com.lepe.myapplication;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

/**
 * Created by LEPE on 9/7/17.
 */

public class SignUpFragment extends Fragment {
    // Progress Dialog Object
    ProgressDialog prgDialog;
    // Error Msg TextView Object
    TextView errorMsg;
    EditText edtTxtEmail, edtTxtPassword, edtTxtName, edtTxtAge, edtTxtHeight, edtTxtWeight;
    Button btnSignUp;
    ImageView imgVwHideUnhide;
    boolean hiddenPassword;

    public SignUpFragment(){}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View LoginView = inflater.inflate(R.layout.fragment_signup,container, false);
        //errorMsg = (TextView)findViewById(R.id.register_error);

        edtTxtEmail = (EditText) LoginView.findViewById(R.id.edtTxtEmailSignUp);
        edtTxtPassword = (EditText) LoginView.findViewById(R.id.edtTxtPasswordSignUp);
        edtTxtName = (EditText) LoginView.findViewById(R.id.edtTxtNameSignUp);
        edtTxtAge = (EditText) LoginView.findViewById(R.id.edtTxtAgeSignUp);
        edtTxtHeight = (EditText) LoginView.findViewById(R.id.edtTxtHeightSignUp);
        edtTxtWeight = (EditText) LoginView.findViewById(R.id.edtTxtWeightSignUp);

        imgVwHideUnhide = (ImageView) LoginView.findViewById(R.id.imgVwHideUnhide);
        hiddenPassword=true;
        imgVwHideUnhide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(hiddenPassword){
                    imgVwHideUnhide.setImageResource(R.drawable.unhide);
                    hiddenPassword=false;
                    edtTxtPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                }
                else{
                    imgVwHideUnhide.setImageResource(R.drawable.hide);
                    hiddenPassword=true;
                    edtTxtPassword.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
            }
        });

        btnSignUp = (Button) LoginView.findViewById(R.id.btnSignUp);
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

                inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
                registerUser();
            }
        });

        // Instantiate Progress Dialog object
        prgDialog = new ProgressDialog(getContext());
        // Set Progress Dialog Text
        prgDialog.setMessage("Please wait...");
        // Set Cancelable as False
        prgDialog.setCancelable(true);
        return LoginView;
    }



    /**
     * Method gets triggered when Register button is clicked
     *
     *
     */
    public void registerUser(){

        String email = edtTxtEmail.getText().toString();
        String password = edtTxtPassword.getText().toString();
        String name = edtTxtName.getText().toString();
        String age = edtTxtAge.getText().toString();
        String height = edtTxtHeight.getText().toString();
        String weight = edtTxtWeight.getText().toString();

        // Instantiate Http Request Param Object
        //RequestParams params = new RequestParams();

        if(Utility.isNotNull(email) && Utility.isNotNull(password) && Utility.isNotNull(name) &&
                Utility.isNotNull(age) && Utility.isNotNull(height) && Utility.isNotNull(weight)){
            // When Email entered is Valid
            if(Utility.validate(email)){

                JSONObject jdata = new JSONObject();

                try {
                    jdata.put("email", email);
                    jdata.put("password", password);
                    jdata.put("name", name);
                    jdata.put("age", age);
                    jdata.put("height", height);
                    jdata.put("weight", weight);

                } catch (Exception ex) {
                    // json exception
                }


                // Invoke RESTful Web Service with Http parameters
                invokeWS(jdata);
            }
            // When Email is invalid
            else{
                Toast.makeText(getContext(), "Please enter valid email", Toast.LENGTH_LONG).show();
            }
        }
        // When any of the Edit View control left blank
        else{
            Toast.makeText(getContext(), "Please fill the form, don't leave any field blank", Toast.LENGTH_LONG).show();
        }

    }

    /**
     * Method that performs RESTful webservice invocations
     *
     * @param jdata
     */
    public void invokeWS(JSONObject jdata){

        // Make RESTful webservice call using AsyncHttpClient object
        final AsyncHttpClient client = new AsyncHttpClient();

        prgDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                client.cancelRequests(getContext(), true);

            }
        });
        // Show Progress Dialog
        prgDialog.show();

        StringEntity entity=null;
        try {
            entity = new StringEntity(jdata.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        client.post(getContext(), "http://192.168.0.7:3000/api/user", entity, "application/json", new JsonHttpResponseHandler() {
            // When the response returned by REST has Http response code '200'
            @Override
            public void onSuccess(int statusCode, Header[] arg1, JSONObject response) {
                // Hide Progress Dialog
                prgDialog.hide();
                setDefaultValues();
                // Display successfully registered message using Toast
                Toast.makeText(getContext(), "You are successfully registered!", Toast.LENGTH_LONG).show();

            }
            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  String responseString, Throwable throwable) {
                // Hide Progress Dialog
                prgDialog.hide();
                // When Http response code is '404'
                if(statusCode == 404){
                    Toast.makeText(getContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if(statusCode == 500){
                    Toast.makeText(getContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else{
                    Toast.makeText(getContext(), "Unexpected Error occcured! " +
                                    "[Most common Error: Device might not be connected to Internet or remote server is not up and running]",
                            Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    /**
     * Set default values for Edit View controls
     */
    public void setDefaultValues(){
        edtTxtEmail.setText("");
        edtTxtPassword.setText("");
        edtTxtName.setText("");
        edtTxtAge.setText("");
        edtTxtHeight.setText("");
        edtTxtWeight.setText("");
        getActivity().getSupportFragmentManager().popBackStack();
    }

}
