package com.lepe.myapplication;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

public class LoginActivity extends AppCompatActivity {

    LoginFragment loginFragment;
    SignUpFragment signUpFragment;
    ProgressDialog prgDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Instantiate Progress Dialog object
        prgDialog = new ProgressDialog(this);
        // Set Progress Dialog Text
        prgDialog.setMessage("Please wait...");
        // Set Cancelable as False
        prgDialog.setCancelable(true);
        loginFragment = new LoginFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.containerSignInUp, loginFragment, "loginFragment").commit();
        SharedPreferences sp = getSharedPreferences(Constants.INFO_USER_SHAREDPREFERENCE, Context.MODE_PRIVATE);
        String token = sp.getString(Constants.TOKEN, "");
        if(!token.equals("")){
            loginUserWithToken(token);
        }
    }

    public void loginUserWithToken(String token){
        // Make RESTful webservice call using AsyncHttpClient object
        final AsyncHttpClient client = new AsyncHttpClient();
        SharedPreferences sp = getSharedPreferences(Constants.INFO_USER_SHAREDPREFERENCE,Context.MODE_PRIVATE);


        prgDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                client.cancelRequests(getApplicationContext(), true);
            }
        });
        // Show Progress Dialog
        prgDialog.show();

        client.addHeader("x-access-token", token+"");

        client.get(getApplicationContext(), "http://192.168.0.7:3000/api/user/"+sp.getString(Constants.USER_ID_PREF,"nouser"), null, "application/json", new JsonHttpResponseHandler() {
            // When the response returned by REST has Http response code '200'
            @Override
            public void onSuccess(int statusCode, Header[] arg1, JSONObject response) {

                // Hide Progress Dialog
                prgDialog.hide();
                try {
                    if(response.getBoolean("success")){
                        // Display successfully Logged In message using Toast
                        //Toast.makeText(getApplicationContext(), "You are successfully Logged In!", Toast.LENGTH_LONG).show();
                        //setDefaultValues();
                        SharedPreferences sp = getApplicationContext().getSharedPreferences(Constants.INFO_USER_SHAREDPREFERENCE, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sp.edit();
                        //editor.putString(Constants.TOKEN, response.getString("token"));
                        editor.putString(Constants.USER_NAME_PREF, response.getString("user_name"));
                        editor.putFloat(Constants.USER_WEIGHT_PREF, (float)response.getDouble("user_weight"));
                        editor.putFloat(Constants.USER_HEIGHT_PREF, (float)response.getDouble("user_height"));
                        editor.putString(Constants.USER_ID_PREF, response.getString("user_id"));
                        editor.apply();
                        Intent intent = new Intent(getApplicationContext(), InfoActivity.class);
                        startActivity(intent);
                        prgDialog.cancel();
                        finish();
                    }
                    else{
                        Toast.makeText(getApplicationContext(), response.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  String responseString, Throwable throwable) {
                // Hide Progress Dialog
                prgDialog.hide();
                // When Http response code is '404'
                if(statusCode == 404){
                    Toast.makeText(getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if(statusCode == 500){
                    Toast.makeText(getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else{
                    Toast.makeText(getApplicationContext(), "Unexpected Error occcured! " +
                                    "[Most common Error: Device might not be connected to Internet or remote server is not up and running]",
                            Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void onGoToSignUp(View v){
        signUpFragment = new SignUpFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_bottom, R.anim.exit_to_bottom, R.anim.enter_from_bottom, R.anim.exit_to_bottom);
        transaction.replace(R.id.containerSignInUp, signUpFragment, "signUpFragment");
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void onGoToForgotPass(View v){

    }

}

