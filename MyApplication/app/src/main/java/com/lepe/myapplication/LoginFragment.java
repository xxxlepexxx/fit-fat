package com.lepe.myapplication;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

/**
 * Created by LEPE on 9/7/17.
 */

public class LoginFragment extends Fragment {
    ProgressDialog prgDialog;
    EditText edtTxtEmail, edtTxtPassword;
    ImageView imgVwHideUnhide;
    boolean hiddenPassword;

    Button btnLogIn;

    public LoginFragment(){}

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View LoginView = inflater.inflate(R.layout.fragment_login,container, false);
        edtTxtEmail = (EditText) LoginView.findViewById(R.id.edtTxtEmailLogin);
        edtTxtPassword = (EditText) LoginView.findViewById(R.id.edtTxtPasswordLogin);
        imgVwHideUnhide = (ImageView) LoginView.findViewById(R.id.imgVwHideUnhideLogin);
        hiddenPassword=true;
        imgVwHideUnhide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(hiddenPassword){
                    imgVwHideUnhide.setImageResource(R.drawable.unhide);
                    hiddenPassword=false;
                    edtTxtPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                }
                else{
                    imgVwHideUnhide.setImageResource(R.drawable.hide);
                    hiddenPassword=true;
                    edtTxtPassword.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_PASSWORD);
                }
            }
        });

        btnLogIn = (Button) LoginView.findViewById(R.id.btnLogIn);
        btnLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager inputManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

                inputManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
                loginUser();
            }
        });
        // Instantiate Progress Dialog object
        prgDialog = new ProgressDialog(getContext());
        // Set Progress Dialog Text
        prgDialog.setMessage("Please wait...");
        // Set Cancelable as False
        prgDialog.setCancelable(true);
        return LoginView;
    }

    /**
     * Method gets triggered when Register button is clicked
     *
     *
     */
    public void loginUser(){

        String email = edtTxtEmail.getText().toString();
        String password = edtTxtPassword.getText().toString();


        // Instantiate Http Request Param Object
        //RequestParams params = new RequestParams();

        if(Utility.isNotNull(email) && Utility.isNotNull(password)){
            // When Email entered is Valid
            if(Utility.validate(email)){


                // Invoke RESTful Web Service
                invokeWS();
            }
            // When Email is invalid
            else{
                Toast.makeText(getContext(), "Please enter valid email", Toast.LENGTH_LONG).show();
            }
        }
        // When any of the Edit View control left blank
        else{
            Toast.makeText(getContext(), "Missing Email or Password", Toast.LENGTH_LONG).show();
        }

    }


    /**
     * Method that performs RESTful webservice invocations
     *
     *
     */
    public void invokeWS(){

        // Make RESTful webservice call using AsyncHttpClient object
        final AsyncHttpClient client = new AsyncHttpClient();

        prgDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                client.cancelRequests(getContext(), true);

            }
        });
        // Show Progress Dialog
        prgDialog.show();

        client.addHeader("x-access-email", edtTxtEmail.getText().toString());
        client.addHeader("x-access-password", edtTxtPassword.getText().toString());
        client.post(getContext(), "http://192.168.0.7:3000/api/authenticate", null, "application/json", new JsonHttpResponseHandler() {
            // When the response returned by REST has Http response code '200'
            @Override
            public void onSuccess(int statusCode, Header[] arg1, JSONObject response) {

                // Hide Progress Dialog
                prgDialog.hide();
                try {
                    if(response.getBoolean("success")){
                        // Display successfully Logged In message using Toast
                        //Toast.makeText(getContext(), "You are successfully Logged In!", Toast.LENGTH_LONG).show();
                        setDefaultValues();
                        SharedPreferences sp = getContext().getSharedPreferences(Constants.INFO_USER_SHAREDPREFERENCE, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sp.edit();
                        editor.putString(Constants.TOKEN, response.getString("token"));
                        editor.putString(Constants.USER_NAME_PREF, response.getString("user_name"));
                        editor.putFloat(Constants.USER_WEIGHT_PREF, (float)response.getDouble("user_weight"));
                        editor.putFloat(Constants.USER_HEIGHT_PREF, (float)response.getDouble("user_height"));
                        editor.putString(Constants.USER_ID_PREF, response.getString("user_id"));
                        editor.apply();
                        Intent intent = new Intent(getActivity(), InfoActivity.class);
                        startActivity(intent);
                        getActivity().finish();
                    }
                    else{
                        Toast.makeText(getContext(), response.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, Header[] headers,
                                  String responseString, Throwable throwable) {
                // Hide Progress Dialog
                prgDialog.hide();
                // When Http response code is '404'
                if(statusCode == 404){
                    Toast.makeText(getContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if(statusCode == 500){
                    Toast.makeText(getContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else{
                    Toast.makeText(getContext(), "Unexpected Error occcured! " +
                                    "[Most common Error: Device might not be connected to Internet or remote server is not up and running]",
                            Toast.LENGTH_LONG).show();
                }
            }
        });
    }
    /**
     * Set default values for Edit View controls
     */
    public void setDefaultValues(){
        edtTxtEmail.setText("");
        edtTxtPassword.setText("");
    }

}
