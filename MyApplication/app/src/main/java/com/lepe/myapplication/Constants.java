package com.lepe.myapplication;

/**
 * Created by LEPE on 9/14/17.
 */

public class Constants {
    public static String INFO_USER_SHAREDPREFERENCE = "info_user";
    public static String TOKEN = "token";
    public static String USER_NAME_PREF = "user_name";
    public static String USER_WEIGHT_PREF = "user_weight";
    public static String USER_HEIGHT_PREF = "user_height";
    public static String USER_ID_PREF = "user_id";
}
