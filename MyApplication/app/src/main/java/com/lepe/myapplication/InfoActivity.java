package com.lepe.myapplication;

import android.content.SharedPreferences;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.text.DecimalFormat;

public class InfoActivity extends AppCompatActivity {

    TextView txtVwUserName, txtVwIMC, txtVwUserWeight, txtVwUserHeight;
    Float weight, height, IMC;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        txtVwUserName = (TextView) findViewById(R.id.txtVwUserName);
        txtVwIMC = (TextView) findViewById(R.id.txtVwIMC);
        txtVwUserWeight = (TextView) findViewById(R.id.txtVwUserWeight);
        txtVwUserHeight = (TextView) findViewById(R.id.txtVwUserHeight);
        SharedPreferences sp = getSharedPreferences(Constants.INFO_USER_SHAREDPREFERENCE, MODE_PRIVATE);
        txtVwUserName.setText(sp.getString(Constants.USER_NAME_PREF,"NO USERNAME"));
        txtVwUserWeight.setText(sp.getFloat(Constants.USER_WEIGHT_PREF,0f)+" Kg");
        txtVwUserHeight.setText(sp.getFloat(Constants.USER_HEIGHT_PREF,0f)+" mts");
        weight=sp.getFloat(Constants.USER_WEIGHT_PREF,0f);
        height=sp.getFloat(Constants.USER_HEIGHT_PREF,0f);
        IMC = weight/(height*height);
        DecimalFormat decimalFormat = new DecimalFormat("#.#");
        IMC = Float.valueOf(decimalFormat.format(IMC));
        txtVwIMC.setText(IMC+"");

    }

}
